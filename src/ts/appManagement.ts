/*
* appManagement.ts
* This file contains functions to manage the applets.
* It contains functions to set up modals, buttons, and app options.
*/
import $ from 'jquery'
import { getUserParams, setUserParams } from './userParams'

// Set up a modal
export function setModal (id: string, icon: string, title: string, content: HTMLDivElement, size: { width: string, height: string }) {
  const modalDialog = document.createElement('div')
  modalDialog.id = id + '_modal'
  modalDialog.setAttribute('tabindex', '-1')
  modalDialog.setAttribute('role', 'dialog')
  modalDialog.className = 'draggableModal'
  modalDialog.style.width = size.width
  modalDialog.style.height = size.height
  const modalContent = document.createElement('div')
  modalContent.className = 'modal-content'
  modalDialog.appendChild(modalContent)
  const modalHeader = document.createElement('div')
  modalHeader.className = 'modal-header bg-dark text-white'
  modalContent.appendChild(modalHeader)
  const row = document.createElement('div')
  row.className = 'w-100 d-flex flex-row justify-content-between align-items-center p-2 px-3 ps-4'
  modalHeader.appendChild(row)
  const span = document.createElement('span')
  span.className = 'draggable_touch d-block'
  row.appendChild(span)
  const h3 = document.createElement('h3')
  h3.className = 'd-inline-block m-0 d-inline user-select-none'
  h3.innerHTML = `<i class="${icon}"></i>&nbsp;&nbsp;${title}`
  span.appendChild(h3)

  const btnSpan = document.createElement('span')
  btnSpan.className = 'd-inline-block'
  row.appendChild(btnSpan)

  const hideContent = document.createElement('button')
  hideContent.type = 'button'
  hideContent.className = 'btn btn-link hideBtn'
  hideContent.setAttribute('aria-label', 'Close')
  hideContent.innerHTML = '<i class="fa fa-minus"></i>'
  btnSpan.appendChild(hideContent)
  hideContent.onclick = hideModalContent(id)

  const button = document.createElement('button')
  button.type = 'button'
  button.className = 'btn btn-link closeBtn'
  button.setAttribute('aria-label', 'Close')
  button.innerHTML = '<i class="fa fa-times"></i>'
  btnSpan.appendChild(button)

  modalContent.appendChild(content)
  document.body.appendChild(modalDialog)

  $('#' + id + '_modal').draggable({
    cursor: 'move',
    handle: '.modal-header'
  })
  $('#' + id + '_modal>.modal-content>.modal-header').css('cursor', 'move')

  $('#' + id + '_modal .closeBtn').click(function () {
    console.log('close')
    changeBtnColor(id, 50)
    $('#' + id + '_modal').hide()
  })

  $('#' + id + '_modal').hide()

  // Find each iframe and remove its src attribute
  $('#' + id + '_modal iframe').each(function () {
    // Store it in data-src
    const src = $(this).attr('src') as string
    $(this).data('src', src)
    $(this).attr('src', '')
  })
}

// Set up a button in the toolbar
export function setButton (id: string, color: string, icon: string, title: string, link: string) {
  const button = document.createElement('button')
  button.id = id
  button.className = 'toolButton'
  button.style.display = 'block'
  button.setAttribute('data-bs-toggle', 'popover')
  button.setAttribute('data-bs-trigger', 'hover')
  button.setAttribute('data-bs-content', title)
  button.innerHTML = `
        <span class="fa-stack fa-2x mt-2">
        <i class="fa fa-circle fa-stack-2x" style="color:${color}"></i>
        <i class="${icon} fa-stack-1x icon-white"></i>
        </span>
        `
  // If the app has a link, add an globe icon at the bottom right of the button
  if (link !== '') {
    const linkIcon = document.createElement('i')
    linkIcon.className = 'fa fa-globe-europe'
    linkIcon.style.position = 'absolute'
    linkIcon.style.bottom = '3px'
    linkIcon.style.right = '3px'
    linkIcon.style.color = 'black'
    linkIcon.style.backgroundColor = 'white'
    linkIcon.style.padding = '2px'
    linkIcon.style.fontSize = '14px'
    linkIcon.style.borderRadius = '50%'
    button.appendChild(linkIcon)
  }
  $('#inner-scroll-wrap').append(button)
    // Bind the button to the modal
    document.getElementById(id)!.addEventListener('click', function () {
      startModal(id)
    })
}

function startModal (id: string) {
  // Test if the modal is displayed
  if ($('#' + id + '_modal').is(':visible')) {
    if ($('#' + id + '_modal').css('z-index') === '20') {
      $('#' + id + '_modal').hide()
      changeBtnColor(id, 50)
      return
    } else {
      // Other modals to 10
      $('.draggableModal').css('z-index', 10)
      $('#' + id + '_modal').css('z-index', 20)
      // Too heavy ?
      // $('#' + id + '_modal').effect('shake', { times: 2, distance: 5 }, 500);
      return
    }
  }
  // Display the modal
  console.log('startModal')
  $('#' + id + '_modal').show()

  $('.draggableModal').css('z-index', 10)
  $('#' + id + '_modal').css('z-index', 20)

  $('#' + id + '_modal')
    .find('.modal-header')
    .mousedown(function () {
      $('.draggableModal').css('z-index', 3)
      $('.draggableModal').css('opacity', 1)
      $('#' + id + '_modal').css('z-index', 10)
      $('#' + id + '_modal').css('opacity', 0.7)
      $('iframe').css('pointer-events', 'none')
    })

  $('#' + id + '_modal').mouseup(function () {
    $('#' + id + '_modal').css('opacity', 1)
    $('iframe').css('pointer-events', 'auto')
  })

  $('#' + id + '_modal').click(function () {
    $('.draggableModal').css('z-index', 3)
    $('#' + id + '_modal').css('z-index', 10)
  })

  changeBtnColor(id, -50)

  // Find each iframe and set its src attribute to data-src
  $('#' + id + '_modal iframe').each(function () {
    const src = $(this).data('src') as string
    $(this).attr('src', src)
  })
}

// Add an option to the app list in the options menu
export function addOption (id: string, title: string) {
  const div = document.createElement('div')
  div.className = 'form-check form-switch'
  const input = document.createElement('input')
  input.type = 'checkbox'
  input.className = 'form-check-input'
  input.id = 'customSwitch' + id
  input.onclick = function () {
    disableApp(id)
  }
  input.checked = true
  const label = document.createElement('label')
  label.className = 'form-check-label'
  label.setAttribute('for', 'customSwitch' + id)
  label.innerHTML = title
  div.appendChild(input)
  div.appendChild(label)
  $('#appList').append(div)

  // Get the user's preferences
  const userParams = getUserParams('appManagement')
  if (userParams) {
    if (userParams[id] === false) {
      $('#' + id).css('display', 'none')
      // Uncheck the checkbox
      $('#' + 'customSwitch' + id).prop('checked', false)
    }
  }
}

function disableApp (id: string) {
  if ($('#' + id).is(':visible')) {
    $('#' + id).css('display', 'none')
  } else {
    $('#' + id).css('display', 'block')
  }
  // Save the user's preferences
  setUserParams('appManagement', { [id]: $('#' + id).is(':visible') })
}

function hideModalContent (id: string) {
  // When the hide button is clicked, hide #modalContent children except the header
  return function () {
    const $modal = $('#' + id + '_modal')
    if ($('#' + id + '_modal .hideBtn').html() === '<i class="fa fa-plus"></i>') {
      $('#' + id + '_modal .modal-content').children().not('.modal-header').toggle()
      $('#' + id + '_modal .hideBtn').html('<i class="fa fa-minus"></i>')
      // Get the height of the modal
      const height = $modal.data('height')
      // Set the height of the modal
      $modal.css('height', height)
      if ($modal.css('resize') !== 'none') {
        // Resize both horizontally and vertically
        $modal.css('resize', 'both')
      }
    } else {
      $('#' + id + '_modal .modal-content').children().not('.modal-header').toggle()
      $('#' + id + '_modal .hideBtn').html('<i class="fa fa-plus"></i>')
      // Store the height of the modal in data-height
      const height = $modal.css('height')
      $modal.data('height', height)
      // Resize the modal to fit the content
      $modal.css('height', 'auto')
      if ($modal.css('resize') !== 'none') {
        // Resize horizontally only
        $modal.css('resize', 'horizontal')
      }
    }
  }
}

function changeBtnColor (id: string, value: number) {
  // Lighten the background of the button
  const btnColor = $('#' + id + ' .fa-circle').css('color')
  // Color is returned as rgb(X, X, X), extract the values
  const colors = btnColor.match(/\d+/g)
  const color1 = Number(colors![0]) + value
  const color2 = Number(colors![1]!) + value
  const color3 = Number(colors![2]) + value
  $('#' + id + ' .fa-circle').css('color', `rgb(${color1}, ${color2}, ${color3})`)
}
