/*
* main.ts
* This file is the main entry point for the application. It initializes the GUI class and calls the init() method to start the application.
*/

import GUI from './gui'

const gui = new GUI()

gui.init()
