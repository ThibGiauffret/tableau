import { setModal, setButton, addOption } from '../appManagement'
import '../../css/calculators.css'

export class Calculators {
  id: string
  title: string
  icon: string
  color: string
  link: string

  public constructor () {
    this.id = 'calculators'
    this.title = 'Calculatrices'
    this.icon = 'fa-solid fa-calculator'
    this.color = '#558BF2'
    this.link = 'calculators'
  }

  public init () {
    console.log('calculators init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '400px', height: '800px' })
    addOption(this.id, this.title)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.position = 'relative'
    content.style.height = '100%'
    content.style.width = '100%'
    content.id = 'calculatorsApp'

    const main = document.createElement('div')
    main.id = 'main'
    content.appendChild(main)

    const ul = document.createElement('ul')
    ul.className = 'nav nav-pills mt-2 mb-2 justify-content-center'
    ul.id = 'pills-tab'
    ul.setAttribute('role', 'tablist')
    main.appendChild(ul)

    const li1 = document.createElement('li')
    li1.className = 'nav-item'
    ul.appendChild(li1)

    const a1 = document.createElement('a')
    a1.className = 'nav-link active'
    a1.id = 'pills-ti83-tab'
    a1.setAttribute('data-bs-toggle', 'pill')
    a1.href = '#pills-ti83'
    a1.setAttribute('role', 'tab')
    a1.setAttribute('aria-controls', 'pills-profile')
    a1.setAttribute('aria-selected', 'false')
    a1.innerHTML = 'Ti83'
    li1.appendChild(a1)

    const li2 = document.createElement('li')
    li2.className = 'nav-item'
    ul.appendChild(li2)

    const a2 = document.createElement('a')
    a2.className = 'nav-link'
    a2.id = 'pills-home-tab'
    a2.setAttribute('data-bs-toggle', 'pill')
    a2.href = '#pills-numworks'
    a2.setAttribute('role', 'tab')
    a2.setAttribute('aria-controls', 'pills-home')
    a2.setAttribute('aria-selected', 'true')
    a2.innerHTML = 'NumWorks'
    li2.appendChild(a2)

    const div = document.createElement('div')
    div.className = 'tab-content'
    div.id = 'pills-tabContent'
    main.appendChild(div)

    const div1 = document.createElement('div')
    div1.className = 'tab-pane fade'
    div1.id = 'pills-numworks'
    div1.setAttribute('role', 'tabpanel')
    div1.setAttribute('aria-labelledby', 'pills-home-tab')
    div.appendChild(div1)

    const div11 = document.createElement('div')
    div11.id = 'numworksdiv'
    div1.appendChild(div11)

    const iframe1 = document.createElement('iframe')
    iframe1.id = 'numworksframe'
    iframe1.setAttribute('scrolling', 'no')
    iframe1.src = 'https://www.numworks.com/simulator/embed/#sid=1sb6wyd'
    iframe1.width = '100%'
    iframe1.height = '100%'
    div11.appendChild(iframe1)

    const script1 = document.createElement('script')
    script1.src = 'https://www.numworks.com/simulator/embed.js'
    div11.appendChild(script1)

    const div2 = document.createElement('div')
    div2.className = 'tab-pane fade show active'
    div2.id = 'pills-ti83'
    div2.setAttribute('role', 'tabpanel')
    div2.setAttribute('aria-labelledby', 'pills-profile-tab')
    div.appendChild(div2)

    const div21 = document.createElement('div')
    div21.id = 'ti83div'
    div2.appendChild(div21)

    const iframe2 = document.createElement('iframe')
    iframe2.id = 'ti83frame'
    iframe2.src = 'https://www.maclasseti.fr/calculatrice'
    div21.appendChild(iframe2)

    return content
  }
}
