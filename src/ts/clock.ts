import { getUserParams, setUserParams } from './userParams'

let serverTime = false
let interval: number
let intervalCallServer: number
let displaySeconds = false

export async function initClock () {
  await getClockUserParams()
  showTime()
  showDate()
  addClockOption()
  // Toogle the clock option
  const input = document.getElementById('customSwitchClock') as HTMLInputElement
  input.checked = serverTime
  // Toogle the seconds option
  const inputSeconds = document.getElementById('customSwitchSeconds') as HTMLInputElement
  inputSeconds.checked = displaySeconds
}

async function showTime () {
  if (serverTime) {
    const date = await distantTime()
    // Add one second to the date
    interval = window.setInterval(function () {
      date.setSeconds(date.getSeconds() + 1)
      displayTime(date)
    }, 1000)
    // Call the server every 15 minutes to update the time
    intervalCallServer = window.setInterval(async function () {
      clearInterval(interval)
      clearInterval(intervalCallServer)
      showTime()
    }, 900000)
  } else {
    let date = new Date()
    // Update the time every second, with computer time
    interval = window.setInterval(function () {
      date = new Date()
      displayTime(date)
    }, 1000)
  }
}

function displayTime (date: Date) {
  let h = date.getHours() // 0 - 23
  const m = date.getMinutes() // 0 - 59
  const s = date.getSeconds() // 0 - 59
  let hStr = ''
  let mStr = ''
  let sStr = ''
  const session = ''

  if (h === 0) {
    h = 12
  }

  hStr = h < 10 ? '0' + h.toString() : h.toString()
  mStr = m < 10 ? '0' + m.toString() : m.toString()
  sStr = s < 10 ? '0' + s.toString() : s.toString()

  let time = ''
  if (displaySeconds) {
    time = hStr + ':' + mStr + ':' + sStr + ' ' + session
  } else {
    time = hStr + ':' + mStr + ' ' + session
  }
  const timer = document.getElementById('timer')
  timer!.innerHTML = time
}

function showDate () {
  const date = new Date()

  const dateLocale = date.toLocaleString('fr-FR', {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  })
  const dateDiv = document.getElementById('date')
  dateDiv!.innerHTML = dateLocale
}

// Add an option to the app options in the options menu
function addClockOption () {
  // Online time option
  const div = document.createElement('div')
  div.className = 'form-check form-switch'
  const input = document.createElement('input')
  input.type = 'checkbox'
  input.className = 'form-check-input'
  input.id = 'customSwitchClock'
  input.checked = false
  input.onclick = function () {
    if (input.checked) {
      serverTime = true
    } else {
      serverTime = false
    }
    // Save the user parameters
    addClockUserParams()
    // Kill the current interval
    clearInterval(interval)
    clearInterval(intervalCallServer)
    showTime()
  }
  const label = document.createElement('label')
  label.className = 'form-check-label'
  label.setAttribute('for', 'customSwitchClock')
  label.innerHTML = 'Heure du serveur'
  div.appendChild(input)
  div.appendChild(label)
  const appOptions = document.getElementById('appOptions')
  appOptions!.appendChild(div)

  // Hide the seconds option
  const divSeconds = document.createElement('div')
  divSeconds.className = 'form-check form-switch'
  const inputSeconds = document.createElement('input')
  inputSeconds.type = 'checkbox'
  inputSeconds.className = 'form-check-input'
  inputSeconds.id = 'customSwitchSeconds'
  inputSeconds.checked = false
  inputSeconds.onclick = function () {
    displaySeconds = !displaySeconds
    // Save the user parameters
    addClockUserParams()
  }
  const labelSeconds = document.createElement('label')
  labelSeconds.className = 'form-check-label'
  labelSeconds.setAttribute('for', 'customSwitchSeconds')
  labelSeconds.innerHTML = 'Afficher les secondes'
  divSeconds.appendChild(inputSeconds)
  divSeconds.appendChild(labelSeconds)
  appOptions!.appendChild(divSeconds)
}

// Update the time by asking the server
async function distantTime (): Promise<Date> {
  console.log('Fetching time from distant server')
  const response = await fetch('http://worldtimeapi.org/api/timezone/Europe/Paris')
  const data = await response.json()
  if (!data.datetime) {
    console.log('Error fetching time from distant server')
    return new Date()
  }
  // Convert data.datetime to a Date object and dont correct the time zone
  const date = new Date(data.datetime)
  return date
}

async function getClockUserParams () {
  const userParams = await getUserParams('clock')
  if (userParams) {
    serverTime = userParams.serverTime
    displaySeconds = userParams.displaySeconds
  }
}

async function addClockUserParams () {
  const clock = {
    serverTime,
    displaySeconds
  }
  setUserParams('clock', clock)
}
