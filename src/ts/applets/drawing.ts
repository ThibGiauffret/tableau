import { setModal, setButton, addOption } from '../appManagement'

export class Drawing {
  id: string
  title: string
  icon: string
  color: string
  link: string
  styles: any

  public constructor () {
    this.id = 'drawing'
    this.title = 'Dessin'
    this.icon = 'fas fa-pen'
    this.color = '#558BF2'
    this.link = ''
    this.styles = {}
  }

  public init () {
    console.log('drawing init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '500px', height: '400px' })
    addOption(this.id, this.title)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.position = 'relative'
    content.style.height = '100%'
    content.style.width = '100%'
    content.id = 'drawingApp'

    // Iframe
    const iframe = document.createElement('iframe')
    iframe.style.width = '100%'
    iframe.style.height = '100%'
    iframe.style.border = 'none'
    iframe.src = './lib/excalidraw/index.html'
    content.appendChild(iframe)

    return content
  }
}
