import { setModal, setButton, addOption } from '../appManagement'

export class EquationEditor {
  id: string
  title: string
  icon: string
  color: string
  link: string

  public constructor () {
    this.id = 'equationEditor'
    this.title = 'Éditeur d\'équations'
    this.icon = 'fa-solid fa-square-root-variable'
    this.color = '#558BF2'
    this.link = 'https://www.ensciences.fr/addons/mathjax/index.html'
  }

  public init () {
    console.log('drawing init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '500px', height: '400px' })
    addOption(this.id, this.title)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.position = 'relative'
    content.style.height = '100%'
    content.style.width = '100%'
    content.id = 'mathjaxApp'

    // Iframe
    const iframe = document.createElement('iframe')
    iframe.style.width = '100%'
    iframe.style.height = '100%'
    iframe.style.border = 'none'
    iframe.src = this.link
    iframe.id = 'mathjaxIframe'
    content.appendChild(iframe)

    return content
  }
}
