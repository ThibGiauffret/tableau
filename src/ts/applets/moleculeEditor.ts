import { setModal, setButton, addOption } from '../appManagement'

export class MoleculeEditor {
  id: string
  title: string
  icon: string
  color: string
  link: string

  public constructor () {
    this.id = 'moleculeEditor'
    this.title = 'Éditeur de molécules'
    this.icon = 'fa-solid fa-flask'
    this.color = '#558BF2'
    this.link = 'https://www.ensciences.fr/addons/molpaintjs/index.html'
  }

  public init () {
    console.log('drawing init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '650px', height: '620px' })
    addOption(this.id, this.title)

    const modal = document.getElementById(this.id + '_modal')
    modal!.style.resize = 'none'
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.position = 'relative'
    content.style.height = '100%'
    content.style.width = '100%'
    content.id = 'moleculeApp'

    // Iframe
    const iframe = document.createElement('iframe')
    iframe.style.width = '100%'
    iframe.style.height = '100%'
    iframe.style.border = 'none'
    iframe.src = this.link
    iframe.id = 'moleculeIframe'
    content.appendChild(iframe)

    return content
  }
}
