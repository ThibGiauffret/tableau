export function getUserParams (app: string = '') {
  if (app) {
    // Get the user parameters from the local storage, for a specific app
    const userParams = localStorage.getItem('userParams')
    if (userParams) {
      return JSON.parse(userParams)[app]
    } else {
      return {}
    }
  } else {
    // Return all the user parameters
    const userParams = localStorage.getItem('userParams')
    if (userParams) {
      return JSON.parse(userParams)
    } else {
      return {}
    }
  }
}

export function setUserParams (app: string, params: any) {
  // Set the user parameters in the local storage for a specific app (add if not exist, update if exist)
  const userParams = localStorage.getItem('userParams')
  if (userParams) {
    const userParamsObj = JSON.parse(userParams)
    // Check if the params already exist in app
    if (userParamsObj[app]) {
      // Update the params
      userParamsObj[app] = { ...userParamsObj[app], ...params }
    } else {
      // Add the params
      userParamsObj[app] = params
    }
    localStorage.setItem('userParams', JSON.stringify(userParamsObj))
  } else {
    const userParamsObj = JSON.parse('{}')
    userParamsObj[app] = params
    localStorage.setItem('userParams', JSON.stringify(userParamsObj))
  }
}
