import { setModal, setButton, addOption } from '../appManagement'
import '../../css/embedViewer.css'

export class EmbedViewer {
  id: string
  title: string
  icon: string
  color: string
  link: string
  tabNumber: number

  public constructor () {
    this.id = 'embedViewer'
    this.title = 'Lecteur d\'URL'
    this.icon = 'fas fa-window-restore'
    this.color = '#558BF2'
    this.link = ''
    this.tabNumber = 1
  }

  public init () {
    console.log('EmbedViewer init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '500px', height: '550px' })
    addOption(this.id, this.title)
  }

  public content () {
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid'
    content.id = 'embedViewerApp'

    const div = document.createElement('div')
    div.id = 'embedControls'
    div.className = 'input-group my-2'

    const input = document.createElement('input')
    input.type = 'text'
    input.className = 'form-control form-control-sm'
    input.placeholder = 'URL de la ressource'
    input.setAttribute('aria-describedby', 'urlButton')
    input.id = 'embedURL'

    const button = document.createElement('button')
    button.className = 'btn btn-success'
    button.type = 'button'
    button.id = 'embedSearchButton'
    button.dataset.bsToggle = 'popover'
    button.dataset.bsTrigger = 'hover'
    button.dataset.bsPlacement = 'bottom'
    button.dataset.bsContent = 'Chercher'
    button.innerHTML = '<i class="fa-solid fa-arrow-right"></i>'

    const resetButton = document.createElement('button')
    resetButton.className = 'btn btn-danger d-none'
    resetButton.type = 'button'
    resetButton.id = 'embedResetButton'
    resetButton.dataset.bsToggle = 'popover'
    resetButton.dataset.bsTrigger = 'hover'
    resetButton.dataset.bsPlacement = 'bottom'
    resetButton.dataset.bsContent = 'Réinitialiser'
    resetButton.innerHTML = '<i class="fa-solid fa-xmark"></i>'

    const addTabButton = document.createElement('button')
    addTabButton.className = 'btn btn-primary'
    addTabButton.type = 'button'
    addTabButton.id = 'addTabButton'
    addTabButton.dataset.bsToggle = 'popover'
    addTabButton.dataset.bsTrigger = 'hover'
    addTabButton.dataset.bsPlacement = 'bottom'
    addTabButton.dataset.bsContent = 'Ajouter un onglet'
    addTabButton.innerHTML = '<i class="fa-solid fa-plus"></i>'

    // Listen for enter key
    input.addEventListener('keypress', (e) => {
      if (e.key === 'Enter') {
        this.search()
      }
    })

    button.addEventListener('click', () => {
      this.search()
    })

    resetButton.addEventListener('click', () => {
      // Find the active tab
      const tabContent = document.querySelector('.tab-pane.active')!
      const iframe = tabContent.querySelector('iframe')!
      const placeholder = tabContent.querySelector('.iframe-placeholder')!
      const input = <HTMLInputElement>document.getElementById('embedURL')
      iframe.src = ''
      iframe.dataset.url = ''
      input.value = ''
      iframe.classList.add('d-none')
      placeholder.classList.remove('d-none')
      resetButton.classList.add('d-none')
    })

    addTabButton.addEventListener('click', () => {
      this.tabNumber++
      // Display the navTabs
      const navTabs = document.getElementById('nav-tab')!
      navTabs.style.display = 'flex'
      const tabContent = document.getElementById('nav-tabContent')!
      tabContent.style.height = 'calc(100% - 85px)'
      // Remove all active classes
      const tabs = document.querySelectorAll('.nav-link')
      tabs.forEach(tab => {
        tab.classList.remove('active')
      })
      const tabContents = document.querySelectorAll('.tab-pane')
      tabContents.forEach(tabContent => {
        tabContent.classList.remove('active')
      })
      // Add new tab content
      const newTabContent = this.buildTabContent()
      tabContent.appendChild(newTabContent)
      // Add new tab
      const newTab = this.buildTab()
      navTabs.appendChild(newTab)
      // Display new tab
      newTab.click()
    })

    div.appendChild(addTabButton)
    div.appendChild(input)
    div.appendChild(resetButton)
    div.appendChild(button)
    content.appendChild(div)

    const navTabs = document.createElement('nav')
    navTabs.id = 'nav-tab'
    navTabs.className = 'nav nav-pills mb-2'
    navTabs.setAttribute('role', 'tablist')
    content.appendChild(navTabs)

    // Hide the navTabs if there is only one tab
    navTabs.style.display = 'none'

    const tabContent = document.createElement('div')
    tabContent.id = 'nav-tabContent'
    tabContent.className = 'tab-content'
    tabContent.style.height = 'calc(100% - 55px)'
    content.appendChild(tabContent)

    tabContent.appendChild(this.buildTabContent())
    navTabs.appendChild(this.buildTab())

    return content
  }

  private search () {
    const url = (<HTMLInputElement>document.getElementById('embedURL')).value
    // Find the active tab
    const tabContent = document.querySelector('.tab-pane.active')!
    const iframe = tabContent.querySelector('iframe')!
    const placeholder = tabContent.querySelector('.iframe-placeholder')!
    const resetButton = document.getElementById('embedResetButton')!
    iframe.src = url
    iframe.dataset.url = url
    iframe.classList.remove('d-none')
    placeholder.classList.add('d-none')
    resetButton.classList.remove('d-none')
  }

  // Build a tab
  private buildTab () {
    const tab = document.createElement('a')
    tab.className = 'nav-link active'
    tab.setAttribute('data-bs-toggle', 'tab')
    tab.setAttribute('data-bs-target', '#tab' + this.tabNumber)
    tab.href = '#tab' + this.tabNumber
    tab.innerHTML = this.tabNumber.toString()

    tab.addEventListener('click', () => {
      // Find the iframe in the tab, and update the input value
      const input = <HTMLInputElement>document.getElementById('embedURL')
      const tabContent = document.querySelector('.tab-pane.active')!
      const iframe = tabContent.querySelector('iframe')!
      input.value = iframe.dataset.url || ''
      // If the iframe is empty, hide the reset button
      const resetButton = document.getElementById('embedResetButton')
      if (iframe.dataset.url === '') {
        resetButton!.classList.add('d-none')
      } else {
        resetButton!.classList.remove('d-none')
      }
    })

    return tab
  }

  // Build a tab content
  private buildTabContent () {
    const tabContent = document.createElement('div')
    tabContent.className = 'tab-pane active'
    tabContent.id = 'tab' + this.tabNumber
    tabContent.setAttribute('role', 'tabpanel')

    // Iframe container
    const iframeContainer = document.createElement('div')
    iframeContainer.className = 'iframe-container'
    tabContent.appendChild(iframeContainer)

    // Placeholder
    const placeholder = document.createElement('div')
    placeholder.className = 'iframe-placeholder'
    placeholder.innerHTML = '<i class="fa-solid fa-arrow-up"></i><br><span class="mt-2">Entrer l\'adresse d\'une ressource</span>'
    iframeContainer.appendChild(placeholder)

    // Iframe
    const iframe = document.createElement('iframe')
    iframe.className = 'responsive-iframe d-none'
    iframe.src = ''
    iframe.dataset.url = ''
    iframeContainer.appendChild(iframe)

    return tabContent
  }
}
