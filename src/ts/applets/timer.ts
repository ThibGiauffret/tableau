import { setModal, setButton, addOption } from '../appManagement'
import '../../css/timer.css'
import mp3File from '../../assets/sounds/bell.mp3'
import $ from 'jquery'

export class Timer {
  id: string
  title: string
  icon: string
  color: string
  link: string
  FULL_DASH_ARRAY = 283
  volume: boolean
  colorCodes: any
  hours: number
  minutes: number
  seconds: number
  timeLimit: number
  timePassed: number
  timeLeft: number
  timerInterval: any
  remainingPathColor: string
  running: boolean

  public constructor () {
    this.id = 'chrono'
    this.title = 'Compte à rebours'
    this.icon = 'fas fa-hourglass-end'
    this.color = '#558BF2'
    this.link = ''

    this.volume = true
    this.colorCodes = {
      info: {
        color: 'green'
      },
      warning: {
        color: 'orange',
        threshold: (15 * 60) / 3
      },
      alert: {
        color: 'red',
        threshold: (15 * 60) / 9
      }
    }
    this.hours = 0
    this.minutes = 0
    this.seconds = 0
    this.timeLimit = 0
    this.timePassed = 0
    this.timeLeft = this.timeLimit
    this.timerInterval = null
    this.remainingPathColor = this.colorCodes.info.color
    this.running = false
    this.plusOneMinute = this.plusOneMinute.bind(this)
    this.minusOneMinute = this.minusOneMinute.bind(this)
    this.displayTime = this.displayTime.bind(this)
    this.startTimer = this.startTimer.bind(this)
    this.resetTimer = this.resetTimer.bind(this)
    this.formatTime = this.formatTime.bind(this)
    this.volumeTimer = this.volumeTimer.bind(this)
  }

  public init () {
    console.log('timer init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '350px', height: '500px' })
    addOption(this.id, this.title)

        document.getElementById('timerHeures')!.addEventListener('input', () => {
          this.displayTime()
        })

        document.getElementById('timerMinutes')!.addEventListener('input', () => {
          this.displayTime()
        })

        document.getElementById('timerSecondes')!.addEventListener('input', () => {
          this.displayTime()
        })

        this.displayTime()
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid p-0'
    content.id = 'timerApp'

    // Settings with inputs to select time
    const collapse = document.createElement('div')
    collapse.className = 'collapse'
    collapse.id = 'timerSettings'
    const card = document.createElement('div')
    card.className = 'card card-body border-0 p-1 m-1 ml-2 mr-2'
    const row = document.createElement('div')
    row.className = 'd-flex flex-row'
    const col1 = document.createElement('div')
    col1.className = 'd-flex flex-row'
    const input1 = document.createElement('input')
    input1.type = 'number'
    input1.className = 'form-control form-control-sm'
    input1.style.width = '70%'
    input1.value = '0'
    input1.min = '0'
    input1.id = 'timerHeures'
    const span1 = document.createElement('span')
    span1.className = 'mt-1 ml-1'
    span1.innerHTML = '&nbsp;h'
    col1.appendChild(input1)
    col1.appendChild(span1)
    const col2 = document.createElement('div')
    col2.className = 'd-flex flex-row'
    const input2 = document.createElement('input')
    input2.type = 'number'
    input2.className = 'form-control form-control-sm'
    input2.style.width = '70%'
    input2.value = '15'
    input2.min = '0'
    input2.max = '60'
    input2.id = 'timerMinutes'
    const span2 = document.createElement('span')
    span2.className = 'mt-1 ml-1'
    span2.innerHTML = '&nbsp;m'
    col2.appendChild(input2)
    col2.appendChild(span2)
    const col3 = document.createElement('div')
    col3.className = 'd-flex flex-row'
    const input3 = document.createElement('input')
    input3.type = 'number'
    input3.className = 'form-control form-control-sm'
    input3.style.width = '70%'
    input3.value = '0'
    input3.min = '0'
    input3.max = '60'
    input3.id = 'timerSecondes'
    const span3 = document.createElement('span')
    span3.className = 'mt-1 ml-1'
    span3.innerHTML = '&nbsp;s'
    col3.appendChild(input3)
    col3.appendChild(span3)
    row.appendChild(col1)
    row.appendChild(col2)
    row.appendChild(col3)
    card.appendChild(row)
    collapse.appendChild(card)
    content.appendChild(collapse)

    // Timer ring svg object
    const baseTimer = document.createElement('div')
    baseTimer.className = 'base-timer'
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    svg.setAttribute('class', 'base-timer__svg')
    svg.setAttribute('viewBox', '0 0 100 100')
    svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg')
    svg.innerHTML = `
        <g class="base-timer__circle">
        <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45"></circle>
        <path
            id="base-timer-path-remaining"
            stroke-dasharray="283"
            class="base-timer__path-remaining ${this.remainingPathColor}"
            d="
            M 50, 50
            m -45, 0
            a 45,45 0 1,0 90,0
            a 45,45 0 1,0 -90,0
            "
        ></path>
        </g>
        `
    baseTimer.appendChild(svg)

    // Timer label
    const baseTimerLabel = document.createElement('span')
    baseTimerLabel.id = 'base-timer-label'
    baseTimerLabel.className = 'base-timer__label'
    baseTimerLabel.innerHTML = this.formatTime(this.timeLeft)
    baseTimer.appendChild(baseTimerLabel)

    content.appendChild(baseTimer)

    // Plus/minus one minute buttons
    const plusminusone = document.createElement('div')
    plusminusone.className = 'plusminusone'
    const btnGroup = document.createElement('div')
    btnGroup.className = 'btn-group'
    btnGroup.setAttribute('role', 'group')
    btnGroup.setAttribute('aria-label', 'Basic example')
    const minusOne = document.createElement('button')
    minusOne.id = 'minusone'
    minusOne.className = 'btn btn-sm btn-light'
    minusOne.innerHTML = '-1 min'
    minusOne.addEventListener('click', this.minusOneMinute)
    const plusOne = document.createElement('button')
    plusOne.id = 'plusone'
    plusOne.className = 'btn btn-sm btn-light'
    plusOne.innerHTML = '+1 min'
    plusOne.addEventListener('click', this.plusOneMinute)
    btnGroup.appendChild(minusOne)
    btnGroup.appendChild(plusOne)
    plusminusone.appendChild(btnGroup)
    content.appendChild(plusminusone)

    // Controls button group
    const dFlex = document.createElement('div')
    dFlex.className = 'd-flex justify-content-center'
    dFlex.id = 'controlGroup'
    const btnGroup2 = document.createElement('div')
    btnGroup2.className = 'btn-group'
    btnGroup2.setAttribute('role', 'group')
    const collapseButton = document.createElement('button')
    collapseButton.className = 'btn btn-primary'
    collapseButton.setAttribute('data-bs-toggle', 'collapse')
    collapseButton.setAttribute('data-bs-target', '#timerSettings')
    collapseButton.setAttribute('aria-expanded', 'false')
    collapseButton.setAttribute('aria-controls', 'timerSettings')
    collapseButton.innerHTML = '<i class="fa-solid fa-sliders"></i>'
    const startButton = document.createElement('button')
    startButton.id = 'buttonTimer'
    startButton.className = 'btn btn-primary'
    startButton.addEventListener('click', this.startTimer)
    startButton.innerHTML = '<i class="fa-solid fa-play"></i>'
    const resetButton = document.createElement('button')
    resetButton.id = 'buttonReset'
    resetButton.className = 'btn btn-primary'
    resetButton.addEventListener('click', this.resetTimer)
    resetButton.innerHTML = '<i class="fa-solid fa-arrows-rotate"></i>'
    const volumeButton = document.createElement('button')
    volumeButton.id = 'volumeTimer'
    volumeButton.className = 'btn btn-primary'
    volumeButton.addEventListener('click', this.volumeTimer)
    volumeButton.innerHTML = '<i class="fa-solid fa-volume-high"></i>'
    btnGroup2.appendChild(collapseButton)
    btnGroup2.appendChild(startButton)
    btnGroup2.appendChild(resetButton)
    btnGroup2.appendChild(volumeButton)
    dFlex.appendChild(btnGroup2)
    content.appendChild(dFlex)

    return content
  }

  // Adds one minute to the timer
  private plusOneMinute () {
    this.timeLimit += 60
    this.timeLeft = this.timeLimit
    $('#base-timer-label').html(this.formatTime(this.timeLeft))
  }

  // Subtract one minute to the timer
  private minusOneMinute () {
    this.timeLimit -= 60
    this.timeLeft = this.timeLimit
    $('#base-timer-label').html(this.formatTime(this.timeLeft))
  }

  // Display time string
  private displayTime () {
    this.hours = parseInt($('#timerHeures').val()!.toString()) * 3600
    this.minutes = parseInt($('#timerMinutes').val()!.toString()) * 60
    this.seconds = parseInt($('#timerSecondes').val()?.toString()!)
    this.timeLimit = Number(this.hours) + Number(this.minutes) + Number(this.seconds)
    this.timeLeft = this.timeLimit
    $('#base-timer-label').html(this.formatTime(this.timeLeft))
  }

  // Handle timer's end
  private async onTimesUp () {
    await this.playSound()
    clearInterval(this.timerInterval)
    this.resetTimer()
  }

  // Handle sound playing at the end of the timer
  // Source : https://bigsoundbank.com/sound-2881-doorbell-9.html
  private async playSound () {
    if (this.volume) {
      const audio = new Audio(mp3File)
      try {
        await audio.play()
      } catch (error) {
        console.error('Failed to play audio:', error)
      }
    }
  }

  // Handle volume on/off
  private volumeTimer () {
    if (this.volume) {
      $('#volumeTimer').html('<i class="fa-solid fa-volume-xmark"></i>')
      this.volume = false
    } else {
      $('#volumeTimer').html('<i class="fa-solid fa-volume-high"></i>')
      this.volume = true
    }
  }

  // Handle the timer
  private startTimer () {
    $('#timerSettings').removeClass('show')
    if (!this.running) {
      this.colorCodes.warning.threshold = this.timeLimit / 3
      this.colorCodes.alert.threshold = this.timeLimit / 9
      this.running = true
      $('#buttonTimer').removeClass('btn-success')
      $('#buttonTimer').addClass('btn-danger')
      $('#buttonTimer').html('<i class="fa-solid fa-pause"></i>')
      this.timerInterval = setInterval(() => {
        this.timePassed = this.timePassed += 1
        this.timeLeft = this.timeLimit - this.timePassed
        $('#base-timer-label').html(this.formatTime(this.timeLeft))
        this.setCircleDasharray()
        this.setremainingPathColor()

        if (this.timeLeft === 0) {
          this.onTimesUp()
        }
      }, 1000)
    } else {
      clearInterval(this.timerInterval)
      this.running = false
      $('#buttonTimer').addClass('btn-success')
      $('#buttonTimer').removeClass('btn-danger')
      $('#buttonTimer').html('<i class="fa-solid fa-play"></i>')
    }
  }

  // Handle reset timer
  private resetTimer () {
    const { alert, warning, info } = this.colorCodes
    clearInterval(this.timerInterval)
    this.timerInterval = null
    this.running = false
    this.hours = parseInt($('#timerHeures').val()!.toString()) * 3600
    this.minutes = parseInt($('#timerMinutes').val()!.toString()) * 60
    this.seconds = parseInt($('#timerSecondes').val()!.toString())
    this.timeLimit = Number(this.hours) + Number(this.minutes) + Number(this.seconds)

    this.timePassed = 0
    this.timeLeft = this.timeLimit
    this.remainingPathColor = info.color
    this.setCircleDasharray()
    this.setremainingPathColor()
    $('#base-timer-path-remaining').prop('classList').add(info.color)
    $('#base-timer-path-remaining').prop('classList').remove(warning.color)
    $('#base-timer-path-remaining').prop('classList').remove(alert.color)
    $('#base-timer-label').html(this.formatTime(this.timeLeft))
    $('#buttonTimer').addClass('btn-success')
    $('#buttonTimer').removeClass('btn-danger')
    $('#buttonTimer').html('<i class="fa-solid fa-play"></i>')
  }

  // Format time, adding zeros when num<10
  private formatTime (time: number) {
    this.minutes = Math.floor(time / 60)
    this.seconds = time % 60
    let seconds = ''
    if (this.seconds < 10) {
      seconds = `0${this.seconds}`
    } else {
      seconds = this.seconds.toString()
    }

    return `${this.minutes}:${seconds}`
  }

  // Handle ring color
  private setremainingPathColor () {
    const { alert, warning, info } = this.colorCodes
    if (this.timeLeft <= alert.threshold) {
      $('#base-timer-path-remaining').prop('classList').remove(warning.color)
      $('#base-timer-path-remaining').prop('classList').add(alert.color)
    } else if (this.timeLeft <= warning.threshold) {
      $('#base-timer-path-remaining').prop('classList').remove(info.color)
      $('#base-timer-path-remaining').prop('classList').add(warning.color)
    }
  }

  private calculateTimeFraction () {
    const rawTimeFraction = this.timeLeft / this.timeLimit
    return rawTimeFraction - (1 / this.timeLimit) * (1 - rawTimeFraction)
  }

  private setCircleDasharray () {
    const circleDasharray = `${(
            this.calculateTimeFraction() * this.FULL_DASH_ARRAY
        ).toFixed(0)} 283`
    $('#base-timer-path-remaining').attr('stroke-dasharray', circleDasharray)
  }
}
