import { setModal, setButton, addOption } from '../appManagement'
import $ from 'jquery'
import diceSound from '../../assets/sounds/dice.mp3'
import '../../css/dice.css'

export class RandomDice {
  id: string
  title: string
  icon: string
  color: string
  link: string

  constructor () {
    this.id = 'randomDice'
    this.title = 'Lancer de dés'
    this.icon = 'fas fa-dice'
    this.color = '#558BF2'
    this.link = ''
  }

  public init () {
    console.log('randomDice init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '500px', height: '380px' })
    addOption(this.id, this.title)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid p-0'
    content.id = 'randomDiceApp'

    const formGroup = document.createElement('div')
    formGroup.style.position = 'absolute'
    formGroup.style.bottom = '10'
    formGroup.style.left = '25'
    formGroup.style.zIndex = '100'
    formGroup.style.width = 'calc(100% - 50px)'
    formGroup.className = 'form-group'
    content.appendChild(formGroup)

    const inputgroup = document.createElement('div')
    inputgroup.className = 'input-group'
    formGroup.appendChild(inputgroup)

    const span = document.createElement('span')
    span.className = 'input-group-text'
    span.innerHTML = '<i class="fas fa-dice"></i>'
    inputgroup.appendChild(span)

    const input = document.createElement('input')
    input.className = 'form-control form-control-sm'
    input.id = 'diceNumber'
    input.type = 'number'
    input.min = '1'
    input.max = '10'
    input.value = '1'
    input.dataset.bsToggle = 'tooltip'
    input.dataset.bsPlacement = 'top'
    input.title = 'Nombre de dés à lancer'
    inputgroup.appendChild(input)

    const button = document.createElement('button')
    button.className = 'btn btn-sm btn-primary'
    button.innerHTML = '<i class="fas fa-play"></i>'
    button.type = 'button'
    button.id = 'rollDice'
    button.addEventListener('click', () => this.rollDice())
    inputgroup.appendChild(button)

    const result = document.createElement('div')
    result.id = 'resultDice'
    result.className = 'd-flex flex-wrap justify-content-center pb-4 align-items-center'
    content.appendChild(result)

    return content
  }

  private async rollDice () {
    const button = document.getElementById('rollDice')
        button!.innerHTML = '<i class="fas fa-spinner fa-spin"></i>'
        const diceNumber = parseInt((document.getElementById('diceNumber') as HTMLInputElement).value)
        // Animation roll randomly between 1 and 6 for each dice
        const result = document.getElementById('resultDice')
        result!.innerHTML = ''
        const results = []
        for (let i = 0; i < diceNumber; i++) {
          results.push(Math.floor(Math.random() * 6) + 1)
        }
        await this.animateRandom(diceNumber, results)
  }

  private async animateRandom (diceNumber: number, results: number[]) {
    const result = document.getElementById('resultDice')
    // Add the number of dice to roll
    for (let i = 0; i < diceNumber; i++) {
            result!.innerHTML += '<i class="fas fa-dice-one p-2"></i>'
    }

    // Shake the modal
    $('#' + this.id + '_modal').effect('shake', { times: 2, distance: 10 }, 500)

    // Roll sound
    const audio = new Audio(diceSound)
    audio.play()

    // Roll animation
    let counter = 0
    const that = this
    const t = setInterval(() => {
      counter++
      if (counter > 8) {
        clearInterval(t)
        counter = 0
        $('#resultDice i').each(function (index) {
          const className = that.displayResult(results[index]) as string
          $(this).removeClass().addClass(className)
          $(this).css({ transform: 'rotate(0deg) translate(0, 0)' })

          $(this).addClass('resultDiceColor')
        })
        const button = document.getElementById('rollDice')
                button!.innerHTML = '<i class="fas fa-play"></i>'
                return
      }
      $('#resultDice i').each(function () {
        const diceResult = Math.floor(Math.random() * 6) + 1
        const className = that.displayResult(diceResult) as string
        $(this).removeClass().addClass(className)
        $(this).css({ transform: 'rotate(' + Math.floor(Math.random() * 360) + 'deg) translate(' + Math.floor(Math.random() * 30) + 'px, ' + Math.floor(Math.random() * 30) + 'px)' })
        $(this).css({ color: 'gray' })
      })
    }, 100)
  }

  private displayResult (diceResult: number) {
    switch (diceResult) {
      case 1:
        return 'fas fa-dice-one p-2'
      case 2:
        return 'fas fa-dice-two p-2'
      case 3:
        return 'fas fa-dice-three p-2'
      case 4:
        return 'fas fa-dice-four p-2'
      case 5:
        return 'fas fa-dice-five p-2'
      case 6:
        return 'fas fa-dice-six p-2'
      default:
        break
    }
  }
}
