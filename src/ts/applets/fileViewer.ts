import { setModal, setButton, addOption } from '../appManagement'

export class FileViewer {
  id: string
  title: string
  icon: string
  color: string
  link: string

  public constructor () {
    this.id = 'fileViewer'
    this.title = 'Lecteur de fichier'
    this.icon = 'fas fa-file'
    this.color = '#558BF2'
    this.link = ''
  }

  public init () {
    console.log('fileViewer init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '500px', height: '500px' })
    addOption(this.id, this.title)
  }

  public content () {
    // Add a central container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid p-0'

    // Add a button to select a file
    const divButton = document.createElement('div')
    divButton.id = 'divFileButton'
    divButton.className = 'form-group'
    divButton.className = 'd-flex flex-column justify-content-center p-3'

    const titre = document.createElement('p')
    titre.innerHTML = 'Ouvrir un fichier PDF ou OpenDocument (ODT, ODS, ODP) :'
    const button = document.createElement('input')
    button.className = 'mb-3'
    button.id = 'ODFileButton'
    button.innerHTML = 'Sélectionner un fichier'
    button.type = 'file'
    button.onchange = this.openFile
    const message = document.createElement('div')
    message.id = 'messageFileViewer'
    message.className = 'd-none'
    message.innerHTML = 'Veuillez sélectionner un fichier PDF ou OpenDocument (au format ODT, ODS ou ODP) !'

    divButton.appendChild(message)
    divButton.appendChild(titre)
    divButton.appendChild(button)

    // Secondary button is used to go back to the file selection
    const secondaryButton = document.createElement('button')
    secondaryButton.id = 'secondaryButton'
    secondaryButton.type = 'button'
    secondaryButton.innerHTML = '<i class="fas fa-fw fa-file-import"></i>'
    secondaryButton.className = 'd-none'
    secondaryButton.style.position = 'absolute'
    secondaryButton.style.bottom = '40px'
    secondaryButton.style.left = '10px'
    secondaryButton.style.width = '40px'
    secondaryButton.style.height = '40px'
    secondaryButton.style.borderRadius = '30px'
    secondaryButton.style.padding = '0'
    secondaryButton.addEventListener('click', this.showOpenFile)

    // Viewer
    const viewer = document.createElement('iframe')
    viewer.id = 'fileContentViewer'
    viewer.className = 'd-none'
    viewer.style.width = '100%'
    viewer.style.height = '100%'
    viewer.style.overflow = 'auto'

    content.appendChild(secondaryButton)
    content.appendChild(divButton)
    content.appendChild(viewer)

    return content
  }

  public openFile () {
    const file = (document.querySelector('#ODFileButton') as HTMLInputElement).files![0]
    if (!file) {
      console.error('No file selected.')
      return
    }

    //    Check file type. If ODT, ODF or ODS, open with ViewerJS
    if (file.name.split('.').pop() === 'odt' || file.name.split('.').pop() === 'ods' || file.name.split('.').pop() === 'odp') {
      const fileReader = new FileReader()
      let base64File
      // Reading file content when it's loaded
      fileReader.onload = function (event) {
        base64File = event.target!.result
        if (!base64File) {
          console.error('Error while reading file.')
        } else {
          const divButton = document.getElementById('divFileButton')
                    divButton!.className = 'd-none'
                    const secondaryButton = document.getElementById('secondaryButton')
                    secondaryButton!.className = 'btn btn-primary'
                    const iframe = (<HTMLIFrameElement>document.querySelector('#fileContentViewer'))
                    iframe.className = 'd-block'
                    iframe!.src = './lib/ViewerJS/#' + base64File
        }
      }
      fileReader.readAsDataURL(file)
    } else if (file.name.split('.').pop() === 'pdf') {
      // Handle the buttons
      const divButton = document.getElementById('divFileButton')
      divButton!.className = 'd-none'
      const secondaryButton = document.getElementById('secondaryButton')
      secondaryButton!.className = 'btn btn-primary'
      // Display with <object> tag
      const fileURL = URL.createObjectURL(file)
      const iframe = (<HTMLIFrameElement>document.querySelector('#fileContentViewer'))
      iframe.className = 'd-block'
      iframe.src = fileURL
    } else {
      console.error('File type not supported.')
      const message = document.getElementById('messageFileViewer')
            message!.className = 'alert alert-danger'
    }
  }

  public showOpenFile () {
    const divButton = document.getElementById('divFileButton')
        divButton!.className = 'd-flex flex-column justify-content-center p-3'
        const secondaryButton = document.getElementById('secondaryButton')
        secondaryButton!.className = 'd-none'
        const iframe = (<HTMLIFrameElement>document.querySelector('#fileContentViewer'))
        iframe.className = 'd-none'
        const message = document.getElementById('messageFileViewer')
        message!.className = 'd-none'
  }
}
