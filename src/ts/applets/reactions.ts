import { setModal, setButton, addOption } from '../appManagement'
import '../../css/reactions.css'
import applause from '../../assets/sounds/applause.mp3'
import falseSound from '../../assets/sounds/false.mp3'
import toobadSound from '../../assets/sounds/toobad.mp3'
import trueSound from '../../assets/sounds/true.mp3'
import victorySound from '../../assets/sounds/victory.mp3'

export class Reactions {
  id: string
  title: string
  icon: string
  color: string
  link: string
  volume: boolean
  audio: HTMLAudioElement

  public constructor () {
    this.id = 'reactions'
    this.title = 'Réactions'
    this.icon = 'fas fa-smile'
    this.color = '#558BF2'
    this.link = ''
    this.volume = true
    this.audio = new Audio()
  }

  public init () {
    console.log('reactions init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '300px', height: '350px' })
    addOption(this.id, this.title)

    const modal = document.getElementById(this.id + '_modal')
        modal!.style.resize = 'none'
  }

  public content () {
    // Main container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid p-2'
    content.id = 'reactionsApp'

    const volumeBtn = document.createElement('button')
    volumeBtn.className = 'btn btn-sm btn-light'
    volumeBtn.id = 'volumeReactionsBtn'
    volumeBtn.innerHTML = '<i class="fas fa-volume-up"></i>'
    content.appendChild(volumeBtn)
    volumeBtn.addEventListener('click', () => {
      this.volume = !this.volume
      volumeBtn.innerHTML = this.volume ? '<i class="fas fa-volume-up"></i>' : '<i class="fas fa-volume-mute"></i>'
    })

    const reactionContainer = document.createElement('div')
    reactionContainer.className = 'd-flex justify-content-center align-items-center flex-wrap h-100 w-100 p-0'
    reactionContainer.id = 'reactionContainer'
    content.appendChild(reactionContainer)

    const reactionSmile = document.createElement('button')
    reactionSmile.className = 'btnReactions'
    reactionSmile.id = 'reactionSmile'
    reactionSmile.innerHTML = '😀'
    reactionContainer.appendChild(reactionSmile)
    reactionSmile.addEventListener('click', () => {
      this.displayFullscreen('😀')
    })

    const reactionNeutral = document.createElement('button')
    reactionNeutral.className = 'btnReactions'
    reactionNeutral.id = 'reactionNeutral'
    reactionNeutral.innerHTML = '😐'
    reactionContainer.appendChild(reactionNeutral)
    reactionNeutral.addEventListener('click', () => {
      this.displayFullscreen('😐')
    })

    const reactionSad = document.createElement('button')
    reactionSad.className = 'btnReactions'
    reactionSad.id = 'reactionSad'
    reactionSad.innerHTML = '😢'
    reactionContainer.appendChild(reactionSad)
    reactionSad.addEventListener('click', () => {
      this.displayFullscreen('😢')
    })

    const reactionParty = document.createElement('button')
    reactionParty.className = 'btnReactions'
    reactionParty.id = 'reactionParty'
    reactionParty.innerHTML = '🎉'
    reactionContainer.appendChild(reactionParty)
    reactionParty.addEventListener('click', () => {
      this.displayFullscreen('🎉')
    })

    const reactionClap = document.createElement('button')
    reactionClap.className = 'btnReactions'
    reactionClap.id = 'reactionClap'
    reactionClap.innerHTML = '👏'
    reactionContainer.appendChild(reactionClap)
    reactionClap.addEventListener('click', () => {
      this.displayFullscreen('👏')
    })

    const reactionThumbUp = document.createElement('button')
    reactionThumbUp.className = 'btnReactions'
    reactionThumbUp.id = 'reactionThumbUp'
    reactionThumbUp.innerHTML = '👍'
    reactionContainer.appendChild(reactionThumbUp)
    reactionThumbUp.addEventListener('click', () => {
      this.displayFullscreen('👍')
    })

    const reactionThumbDown = document.createElement('button')
    reactionThumbDown.className = 'btnReactions'
    reactionThumbDown.id = 'reactionThumbDown'
    reactionThumbDown.innerHTML = '👎'
    reactionContainer.appendChild(reactionThumbDown)
    reactionThumbDown.addEventListener('click', () => {
      this.displayFullscreen('👎')
    })

    const reactionCheck = document.createElement('button')
    reactionCheck.className = 'btnReactions'
    reactionCheck.id = 'reactionCheck'
    reactionCheck.innerHTML = '✅'
    reactionContainer.appendChild(reactionCheck)
    reactionCheck.addEventListener('click', () => {
      this.displayFullscreen('✅')
    })

    const reactionCross = document.createElement('button')
    reactionCross.className = 'btnReactions'
    reactionCross.id = 'reactionCross'
    reactionCross.innerHTML = '❌'
    reactionContainer.appendChild(reactionCross)
    reactionCross.addEventListener('click', () => {
      this.displayFullscreen('❌')
    })

    return content
  }

  private displayFullscreen (reaction: string, sound: string = '') {
    const fullscreen = document.createElement('div')
    fullscreen.style.position = 'fixed'
    fullscreen.style.top = '0'
    fullscreen.style.left = '0'
    fullscreen.style.width = '100%'
    fullscreen.style.height = '100%'
    fullscreen.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
    fullscreen.style.zIndex = '9999'
    fullscreen.style.display = 'flex'
    fullscreen.style.justifyContent = 'center'
    fullscreen.style.alignItems = 'center'
    fullscreen.style.fontSize = '12rem'
    fullscreen.style.color = 'white'
    fullscreen.style.userSelect = 'none'
    fullscreen.style.cursor = 'pointer'
    fullscreen.innerHTML = reaction
    document.body.appendChild(fullscreen)

    // Play sound
    if (this.volume) {
      switch (reaction) {
        case '😀':
          sound = applause
          break
        case '😐':
          sound = falseSound
          break
        case '😢':
          sound = toobadSound
          break
        case '🎉':
          sound = victorySound
          break
        case '👏':
          sound = applause
          break
        case '👍':
          sound = trueSound
          break
        case '👎':
          sound = falseSound
          break
        case '✅':
          sound = trueSound
          break
        case '❌':
          sound = falseSound
          break
      }
      // Play sound
      this.audio = new Audio(sound)
      this.audio.play()
    }

    fullscreen.addEventListener('click', () => {
      fullscreen.remove()
      if (this.volume) {
        this.audio.pause()
      }
    })
  }
}
