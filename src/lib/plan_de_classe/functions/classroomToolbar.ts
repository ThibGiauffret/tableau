export default class ClassroomToolbar {
  defaultParams: any

  public constructor () {
    this.defaultParams = {
      rows: 11,
      columns: 11,
      pos: [
        [0, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0],
        [1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      ],
      display: {
        zoom: 1
      }
    }
  }

  public getClassroomDefaultParams () {
    return this.defaultParams
  }

  public buildClassroomToolbar () {
    const toolbar = document.createElement('div')
    toolbar.style.width = '100%'
    toolbar.style.height = '100%'
    toolbar.style.overflow = 'auto'
    toolbar.id = 'classroomToolbar'

    // Label
    const label = document.createElement('h5')
    label.innerHTML = 'Configuration de la salle de classe'
    label.className = 'd-block mt-2'
    toolbar.appendChild(label)

    // Add an input to select the number of rows and columns
    const labelDim = document.createElement('label')
    labelDim.innerHTML = 'Dimensions de la salle'
    labelDim.className = 'd-block mt-2'
    toolbar.appendChild(labelDim)

    // Inputgroup for rows and columns
    const inputGroup = document.createElement('div')
    inputGroup.className = 'input-group input-group-sm mb-2'
    toolbar.appendChild(inputGroup)

    const labelRows = document.createElement('label')
    labelRows.innerHTML = '<i class="fas fa-fw fa-ruler-vertical"></i>'
    labelRows.className = 'input-group-text'
    labelRows.setAttribute('data-bs-toggle', 'popover')
    labelRows.setAttribute('data-bs-trigger', 'hover')
    labelRows.setAttribute('data-bs-content', 'Nombre de rangées')
    labelRows.setAttribute('data-bs-placement', 'left')
    inputGroup.appendChild(labelRows)

    const inputRows = document.createElement('input')
    inputRows.id = 'inputRows'
    inputRows.type = 'number'
    inputRows.min = '1'
    inputRows.max = '20'
    inputRows.value = this.defaultParams.rows.toString()
    inputRows.className = 'form-control'
    inputGroup.appendChild(inputRows)

    const inputGroup2 = document.createElement('div')
    inputGroup2.className = 'input-group input-group-sm mb-2'
    toolbar.appendChild(inputGroup2)

    const labelColumns = document.createElement('label')
    labelColumns.innerHTML = '<i class="fas fa-fw fa-ruler-horizontal"></i>'
    labelColumns.className = 'input-group-text'
    labelColumns.setAttribute('data-bs-toggle', 'popover')
    labelColumns.setAttribute('data-bs-trigger', 'hover')
    labelColumns.setAttribute('data-bs-content', 'Nombre de colonnes')
    labelColumns.setAttribute('data-bs-placement', 'left')
    inputGroup2.appendChild(labelColumns)

    const inputColumns = document.createElement('input')
    inputColumns.id = 'inputColumns'
    inputColumns.type = 'number'
    inputColumns.min = '1'
    inputColumns.max = '20'
    inputColumns.value = this.defaultParams.columns.toString()
    inputColumns.className = 'form-control'
    inputGroup2.appendChild(inputColumns)

    // Label for tools
    const labelTools = document.createElement('label')
    labelTools.innerHTML = 'Gestion des tables'
    labelTools.className = 'd-block mt-2'
    toolbar.appendChild(labelTools)

    // Delete table button
    const deleteTableButton = document.createElement('input')
    deleteTableButton.type = 'checkbox'
    deleteTableButton.name = 'classroomTools'
    deleteTableButton.id = 'deleteClassroomTableButton'
    deleteTableButton.className = 'btn-check'
    toolbar.appendChild(deleteTableButton)
    const deleteTableLabel = document.createElement('label')
    deleteTableLabel.className = 'btn btn-sm btn-outline-danger m-1'
    deleteTableLabel.htmlFor = 'deleteClassroomTableButton'
    deleteTableLabel.innerHTML = '<i class="fas fa-trash"></i>'
    deleteTableLabel.setAttribute('data-bs-toggle', 'popover')
    deleteTableLabel.setAttribute('data-bs-trigger', 'hover')
    deleteTableLabel.setAttribute('data-bs-content', 'Supprimer une table')
    deleteTableLabel.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(deleteTableLabel)

    // Add table button
    const addTableButton = document.createElement('input')
    addTableButton.type = 'checkbox'
    addTableButton.name = 'classroomTools'
    addTableButton.id = 'addClassroomTableButton'
    addTableButton.className = 'btn-check'
    toolbar.appendChild(addTableButton)
    const addTableLabel = document.createElement('label')
    addTableLabel.className = 'btn btn-sm btn-outline-success m-1'
    addTableLabel.htmlFor = 'addClassroomTableButton'
    addTableLabel.innerHTML = '<i class="fas fa-plus"></i>'
    addTableLabel.setAttribute('data-bs-toggle', 'popover')
    addTableLabel.setAttribute('data-bs-trigger', 'hover')
    addTableLabel.setAttribute('data-bs-content', 'Ajouter une table')
    addTableLabel.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(addTableLabel)

    // Label for students
    const labelStudents = document.createElement('h5')
    labelStudents.innerHTML = 'Placement des élèves'
    labelStudents.className = 'd-block mt-2'
    toolbar.appendChild(labelStudents)

    // Students list
    const studentsListLabel = document.createElement('label')
    studentsListLabel.className = 'd-block mb-1'
    studentsListLabel.innerHTML = 'Élèves à placer'
    toolbar.appendChild(studentsListLabel)
    // Number of students
    const studentsNumber = document.createElement('span')
    studentsNumber.id = 'studentsNumber'
    studentsNumber.className = 'badge  rounded-pill  bg-primary ms-2'
    studentsNumber.innerHTML = '0'
    studentsListLabel.appendChild(studentsNumber)
    const studentsListContainer = document.createElement('div')
    studentsListContainer.id = 'studentsToolbarListContainer'
    studentsListContainer.className = 'mb-2'
    toolbar.appendChild(studentsListContainer)
    const studentsList = document.createElement('div')
    studentsList.id = 'studentsToolbarList'
    studentsList.className = 'd-flex flex-column p-2'
    studentsListContainer.appendChild(studentsList)

    // Placeholder
    const placeholder = document.createElement('div')
    placeholder.id = 'studentsToolbarListPlaceholder'
    placeholder.className = 'd-block text-center text-muted m-auto p-2'
    // Text with an arrow to the bottom
    placeholder.innerHTML = 'Charger une liste d\'élèves<br> <i class="fas fa-arrow-down"></i>'
    studentsListContainer.appendChild(placeholder)

    // Add an file input to load a list of students from csv file
    const inputLoad = document.createElement('input')
    inputLoad.type = 'file'
    inputLoad.accept = '.csv'
    inputLoad.id = 'inputLoadStudents'
    inputLoad.className = 'form-control form-control-sm mb-2'
    inputLoad.setAttribute('data-bs-toggle', 'popover')
    inputLoad.setAttribute('data-bs-trigger', 'hover')
    inputLoad.setAttribute('data-bs-content', 'Charger une liste d\'élèves depuis un fichier CSV (séparateur \';\' et première colonne contenant les noms des élèves)')
    inputLoad.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(inputLoad)

    // Place students in order
    const buttonOrder = document.createElement('button')
    buttonOrder.innerHTML = '<i class="fas fa-fw fa-sort-alpha-down"></i>'
    buttonOrder.id = 'orderButton'
    buttonOrder.className = 'btn btn-primary m-1'
    buttonOrder.setAttribute('data-bs-toggle', 'popover')
    buttonOrder.setAttribute('data-bs-trigger', 'hover')
    buttonOrder.setAttribute('data-bs-content', 'Placer par ordre alphabétique')
    buttonOrder.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(buttonOrder)

    const buttonRandom = document.createElement('button')
    buttonRandom.innerHTML = '<i class="fas fa-fw fa-random"></i>'
    buttonRandom.id = 'randomizeButton'
    buttonRandom.className = 'btn btn-primary m-1'
    buttonRandom.setAttribute('data-bs-toggle', 'popover')
    buttonRandom.setAttribute('data-bs-trigger', 'hover')
    buttonRandom.setAttribute('data-bs-content', 'Placer aléatoirement')
    buttonRandom.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(buttonRandom)

    const buttonPutAllStudentsInList = document.createElement('button')
    buttonPutAllStudentsInList.innerHTML = '<i class="fas fa-fw fa-redo-alt"></i>'
    buttonPutAllStudentsInList.id = 'putAllStudentsInListButton'
    buttonPutAllStudentsInList.className = 'btn btn-primary m-1'
    buttonPutAllStudentsInList.setAttribute('data-bs-toggle', 'popover')
    buttonPutAllStudentsInList.setAttribute('data-bs-trigger', 'hover')
    buttonPutAllStudentsInList.setAttribute('data-bs-content', 'Remettre les élèves dans la liste')
    buttonPutAllStudentsInList.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(buttonPutAllStudentsInList)

    toolbar.appendChild(document.createElement('br'))

    const buttonFullscreen = document.createElement('button')
    buttonFullscreen.innerHTML = '<i class="fas fa-fw fa-expand"></i>'
    buttonFullscreen.id = 'fullscreenClassroomButton'
    buttonFullscreen.className = 'btn btn-warning m-1'
    buttonFullscreen.setAttribute('data-bs-toggle', 'popover')
    buttonFullscreen.setAttribute('data-bs-trigger', 'hover')
    buttonFullscreen.setAttribute('data-bs-content', 'Afficher en plein écran')
    buttonFullscreen.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(buttonFullscreen)

    const buttonPicture = document.createElement('button')
    buttonPicture.innerHTML = '<i class="fas fa-fw fa-camera"></i>'
    buttonPicture.id = 'pictureButton'
    buttonPicture.className = 'btn btn-warning m-1'
    buttonPicture.setAttribute('data-bs-toggle', 'popover')
    buttonPicture.setAttribute('data-bs-trigger', 'hover')
    buttonPicture.setAttribute('data-bs-content', 'Prendre une capture')
    buttonPicture.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(buttonPicture)

    const buttonExport = document.createElement('button')
    buttonExport.innerHTML = '<i class="fas fa-fw fa-save"></i>'
    buttonExport.id = 'exportButton'
    buttonExport.className = 'btn btn-warning m-1'
    buttonExport.setAttribute('data-bs-toggle', 'popover')
    buttonExport.setAttribute('data-bs-trigger', 'hover')
    buttonExport.setAttribute('data-bs-content', 'Exporter (bientôt disponible)')
    buttonExport.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(buttonExport)

    const buttonImport = document.createElement('button')
    buttonImport.innerHTML = '<i class="fas fa-fw fa-upload"></i>'
    buttonImport.id = 'importButton'
    buttonImport.className = 'btn btn-warning m-1'
    buttonImport.setAttribute('data-bs-toggle', 'popover')
    buttonImport.setAttribute('data-bs-trigger', 'hover')
    buttonImport.setAttribute('data-bs-content', 'Importer (bientôt disponible)')
    buttonImport.setAttribute('data-bs-placement', 'bottom')
    toolbar.appendChild(buttonImport)

    return toolbar as HTMLElement
  }

  public loadCSV (file: File) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onload = function (e) {
        // Get only the first column
        const data = (e.target as FileReader).result as string
        const lines = data.split('\n')
        const students = lines.map(line => line.split(';')[0])
        // Remove the first element (header)
        students.shift()
        resolve(students as string[])
      }
      reader.readAsText(file)
    })
  }
}
