import { setModal, setButton, addOption } from '../appManagement'
import '../../css/chronometer.css'

export class Chronometer {
  id: string
  title: string
  icon: string
  color: string
  link: string
  running: boolean = false
  interval: any

  public constructor () {
    this.id = 'chronometer'
    this.title = 'Chronomètre'
    this.icon = 'fas fa-stopwatch'
    this.color = '#558BF2'
    this.link = ''
  }

  public init () {
    console.log('chronometer init')
    setButton(this.id, this.color, this.icon, this.title, this.link)
    setModal(this.id, this.icon, this.title, this.content(), { width: '400px', height: '230px' })
    addOption(this.id, this.title)

    // Resize observer to change font size
    const resizeObserver = new ResizeObserver(entries => {
      window.requestAnimationFrame((): void | undefined => {
        if (!Array.isArray(entries) || !entries.length) {
          return
        }
        for (const entry of entries) {
          const fontSize = Math.min(entry.contentRect.width / 7, 100)
        document.getElementById('chronoDisplay')!.style.fontSize = fontSize + 'px'
        }
      })
    })

    resizeObserver.observe(document.getElementById('chronoContainer')!)
  }

  private content () {
    // Main container
    const content = document.createElement('div')
    content.style.height = '100%'
    content.className = 'container-fluid p-0'
    content.id = 'chronometerApp'

    const chronoContainer = document.createElement('div')
    chronoContainer.className = 'd-flex justify-content-center align-items-center'
    chronoContainer.id = 'chronoContainer'
    content.appendChild(chronoContainer)

    const chronometer = document.createElement('div')
    chronometer.className = 'd-flex justify-content-center align-items-center text-responsive'
    chronometer.id = 'chronoDisplay'
    chronometer.innerHTML = '00:00:00'
    chronoContainer.appendChild(chronometer)

    const divButtons = document.createElement('div')
    divButtons.className = 'd-flex justify-content-center'
    content.appendChild(divButtons)

    const startButton = document.createElement('button')
    startButton.className = 'btn btn-success m-1'
    startButton.id = 'startChronoButton'
    startButton.innerHTML = '<i class="fas fa-play"></i>'
    startButton.addEventListener('click', () => {
      this.startStopTimer()
    })
    divButtons.appendChild(startButton)

    const resetButton = document.createElement('button')
    resetButton.className = 'btn btn-warning m-1'
    resetButton.id = 'resetButton'
    resetButton.innerHTML = '<i class="fas fa-redo"></i>'
    resetButton.addEventListener('click', () => {
      this.resetTimer()
    })
    divButtons.appendChild(resetButton)

    return content
  }

  private startStopTimer () {
    if (this.running) {
      this.running = false
      clearInterval(this.interval)
      document.getElementById('startChronoButton')!.innerHTML = '<i class="fas fa-play"></i>'
    } else {
      this.running = true
        document.getElementById('startChronoButton')!.innerHTML = '<i class="fas fa-pause"></i>'
        const that = this
        this.interval = setInterval(() => {
          that.updateTimer()
        }, 1000)
    }
  }

  private updateTimer () {
    console.log('update timer')
    const chronometer = document.getElementById('chronoDisplay')!
    const time = chronometer.innerHTML.split(':')
    let hours = parseInt(time[0], 10)
    let minutes = parseInt(time[1], 10)
    let seconds = parseInt(time[2], 10)

    seconds++
    if (seconds === 60) {
      seconds = 0
      minutes++
    }
    if (minutes === 60) {
      minutes = 0
      hours++
    }

    chronometer.innerHTML = `${this.formatTime(hours)}:${this.formatTime(minutes)}:${this.formatTime(seconds)}`
  }

  private formatTime (time: number) {
    return time < 10 ? '0' + time.toString() : time.toString()
  }

  private resetTimer () {
    const chronometer = document.getElementById('chronoDisplay')!
    chronometer.innerHTML = '00:00:00'
    this.running = false
    document.getElementById('startChronoButton')!.innerHTML = '<i class="fas fa-play"></i>'
    clearInterval(this.interval)
  }
}
